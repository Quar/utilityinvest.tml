**This is an archive of UIL.TML, for latest update please visit: [https://github.com/Quar/UIL.Tml/](https://github.com/Quar/UIL.Tml/)**

# README #

This README documents necessary steps to get [Utility Investment Library (UIL)][uil.wiki] setup and running.

[Utility Investment Library (UIL)][uil.wiki] is an [AnyLogic][ext.anylogic] library supports decision making for electric utility expansion. UIL provides a framework to automate [Integrated Resource Plan (IRP)][ext.irp] and perform prestigious statistical inferences for risk analysis.


### What is this repository for? ###

* This repository ([uil.tml][uil.tml]) is an end user repository of [UIL project][uil.about], dedeicate in hosting:
     - published binary UIL library in [Java Archive (JAR)][ext.jar] form
     - published financial model in Excel
* As an end user repository, [uil.tml][uil.tml] contains all resources for all [roles of users][wiki.role].
* Visit our [UIL Wiki][uil.wiki] for more information.


### How do I get set up? ###

* Install [AnyLogic][url.anylogic]
* Install [Utility Investment Library (UIL)][jar.uil]

Please visit our [**Quick Start Guide**][wiki.quickstart] for step-by-step instruction.


### Contribution guidelines ###

* Please visit Financial Model Template [Contribute Guide][wiki.contrib].


### Who do I talk to? ###

All informations are hosted on our [**UIL Wiki**][uil.wiki].

Please visit: https://github.com/Quar/UIL.TML/wiki

Or short URL: http://goo.gl/TPb6iJ


[url.anylogic]: http://www.anylogic.com/downloads
[jar.uil]: https://bitbucket.org/Quar/uil.tml/downloads/UtilityInvestLibrary.jar

[uil.tml]: https://github.com/Quar/uil.tml
[uil.about]: https://github.com/Quar/UIL.TML/wiki/About-and-FAQ.md
[uil.wiki]: https://github.com/Quar/UIL.TML/wiki

[wiki.quickstart]: https://github.com/Quar/UIL.TML/wiki/Quick-Start-Guide
[wiki.role]: https://github.com/Quar/UIL.TML/wiki/Role-playing.md
[wiki.contrib]: https://github.com/Quar/UIL.TML/wiki/Contribute-to-UIL.TML

[ext.jar]: https://en.wikipedia.org/wiki/JAR_%28file_format%29
[ext.irp]: http://www.osti.gov/scitech/biblio/6719825
[ext.anylogic]: https://en.wikipedia.org/wiki/AnyLogic